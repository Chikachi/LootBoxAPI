const rp = require('request-promise');
const cheerio = require('cheerio');
const Joi = require('joi');

const getValue = ($elem) => {
  let value = $elem.html();

  if (value == null) {
    return null;
  }

  value = value.split(',').join('');

  if (!isNaN(value) && isFinite(value)) {
    value = parseFloat(value);
  }

  return value;
};

const getNextValue = ($elem) => getValue($elem.next());

const getProfile = (tag, region, platform, next) => { // eslint-disable-line
  let url = `https://playoverwatch.com/en-us/career/${platform}/${region}/${tag}`;

  if (platform === 'psn' || platform === 'xbl') {
    url = `https://playoverwatch.com/en-us/career/${platform}/${tag}`;
  }

  rp(url)
    .then((htmlString) => {
      const $ = cheerio.load(htmlString);
      const username = $('.header-masthead').text();

      const $quickplay = $('#quickplay');
      const $competitive = $('#competitive');

      const games = {
        quick: {
          wins: getNextValue($quickplay.find('td:contains("Games Won")')),
          lost: null,
          played: getNextValue($quickplay.find('td:contains("Games Played")')),
        },
        competitive: {
          wins: getNextValue($competitive.find('td:contains("Games Won")')),
          lost: null,
          played: getNextValue($competitive.find('td:contains("Games Played")')),
        },
      };
      const playtime = {
        quick: getNextValue($quickplay.find('td:contains("Time Played")')),
        competitive: getNextValue($competitive.find('td:contains("Time Played")')),
      };
      let competitiveRank;
      let competitiveRankImg;
      let star = null;

      const $competitiveRank = $('.competitive-rank');

      const $playerLevel = $('.player-level');

      const levelFrame = $playerLevel.css('background-image').substr(4, -1);

      const $playerRank = $playerLevel.find('.player-rank');
      if ($playerRank.length > 0) {
        star = $playerRank.css('background-image').substr(4, -1);
      }

      if ($competitiveRank != null) {
        competitiveRankImg = $('.competitive-rank img').attr('src');
        competitiveRank = getValue($('.competitive-rank div'));
      }

      if (games.quick.played == null) {
        const data = [
          {
            average: getNextValue($quickplay.find('td:contains("Melee Final Blows - Average")')),
            total: getNextValue($quickplay.find('td:contains("Melee Final Blows")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Solo Kills - Average")')),
            total: getNextValue($quickplay.find('td:contains("Solo Kills")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Objective Kills - Average")')),
            total: getNextValue($quickplay.find('td:contains("Objective Kills")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Healing Done - Average")')),
            total: getNextValue($quickplay.find('td:contains("Healing Done")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Final Blows - Average")')),
            total: getNextValue($quickplay.find('td:contains("Final Blows")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Deaths - Average")')),
            total: getNextValue($quickplay.find('td:contains("Deaths")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Damage Done - Average")')),
            total: getNextValue($quickplay.find('td:contains("Damage Done")')),
          },
          {
            average: getNextValue($quickplay.find('td:contains("Eliminations - Average")')),
            total: getNextValue($quickplay.find('td:contains("Eliminations")')),
          },
        ];

        let current = null;
        let diff = null;

        for (let i = 0, j = data.length; i < j; i++) {
          if (data[i].average != null && data[i].total != null) {
            const newDiff = Math.abs(1 - data[i].average);
            if (diff == null || newDiff < diff) {
              diff = newDiff;
              current = data[i];
            }
          }
        }

        if (current != null) {
          games.quick.played = Math.ceil(current.total / current.average);
          games.quick.lost = games.quick.played - games.quick.wins;
        }
      } else {
        games.quick.lost = games.quick.played - games.quick.wins;
      }

      if (games.competitive.played != null) {
        games.competitive.lost = games.competitive.played - games.competitive.wins;
      }

      rp(`https://playoverwatch.com/en-us/search/account-by-name/${tag}`)
        .then((html) => {
          const profiles = JSON.parse(html);
          let profile;
          let searchString;

          if (platform === 'pc') {
            searchString = `/career/${platform}/${region}/${decodeURI(tag)}`;
          } else {
            searchString = `/career/${platform}/${decodeURI(tag)}`;
          }

          for (let i = 0; i < profiles.length; i++) {
            if (profiles[i].careerLink === searchString) {
              profile = profiles[i];
            }
          }
          /* eslint-disable max-len */

          return next(null, {
            data: {
              username,
              level: profile.level,
              games,
              playtime,
              avatar: profile.portrait,
              competitive: { rank: competitiveRank, rank_img: competitiveRankImg },
              levelFrame,
              star,
            },
          });
          /* eslint-enable max-len */
        });
    }).catch((err) => {
      console.error(err);
      next(null, { statusCode: 404, error: `Found no user with the BattleTag: ${tag}` });
    });
};

exports.register = (server, options, next) => { // eslint-disable-line
  server.method('getProfile', getProfile, {
    cache: {
      cache: 'mongo',
      expiresIn: 6 * 10000, // 10 minutes
      generateTimeout: 40000,
      staleTimeout: 10000,
      staleIn: 20000,
    },
  });

  server.route({
    method: 'GET',
    path: '/{platform}/{region}/{tag}/profile',

    config: {
      tags: ['api'],
      plugins: {
        'hapi-rate-limit': {
          pathLimit: 50,
        },
      },
      validate: {
        params: {
          tag: Joi.string()
            .required()
            // .regex(/^(?:[a-zA-Z\u00C0-\u017F0-9]{3,12}-[0-9]{4,},)?(?:[a-zA-Z\u00C0-\u017F0-9]{3,12}-[0-9]{4,})$/g)
            .description('the battle-tag of the user | "#" should be replaced by an "-"'),
          platform: Joi.string()
            .required()
            .insensitive()
            .valid(['pc', 'xbl', 'psn'])
            .description('the platform that the user use: pc,xbl,psn'),
          region: Joi.string()
            .required()
            .insensitive()
            .valid(['eu', 'us', 'kr', 'cn', 'global'])
            .description('the region the user live is in for example: eu'),
        },
      },
      description: 'Get Stats for a specific hero',
      notes: ' ',
    },
    handler: (request, reply) => {
      const tag = encodeURIComponent(request.params.tag);
      const region = encodeURIComponent(request.params.region);
      const platform = encodeURIComponent(request.params.platform);

      server.methods.getProfile(tag, region, platform, (err, result) => {
        if (err) {
          return reply(err);
        }

        return reply(result);
      });
    },
  });
  return next();
};

exports.register.attributes = {
  name: 'routes-profile',
};
